<?php
/**
 * Copyright (C) 2014 Andreas Jonsson <andreas.jonsson@kreablo.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Extensions
 */

if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

$extDir = __DIR__ ;

$wgExtensionCredits['jqueryhooks'][] = array(
	'path' => __FILE__,
	'name' => 'JQueryHooks',
	'author' => array( 'Andreas Jonsson' ),
	'url' => false,
	'version' => '1.0',
	'descriptionmsg' => 'jqueryhooks-desc',
);

$wgExtensionMessagesFiles['JQueryHooks'] = "$extDir/JQueryHooks.i18n.php";

$wgHooks['ParserFirstCallInit'][] = function( Parser $parser ) {
    $parser->setHook( 'use_accordion', function ( $input, array $args, Parser $parser, PPFrame $frame ) {

        $parser->getOutput()->addModules( 'ext.JQueryHooks.accordion' );

        return '';
    } );

    return true;
};

$wgResourceModules['ext.JQueryHooks.accordion'] = array(
        // JavaScript and CSS styles. To combine multiple files, just list them as an array.
        'scripts' => 'js/use_accordion.js',
        'styles' => 'css/style.css',

        // When your module is loaded, these messages will be available through mw.msg()
        'messages' => array(),

        // If your scripts need code from other modules, list their identifiers as dependencies
        // and ResourceLoader will make sure they're loaded before you.
        // You don't need to manually list 'mediawiki' or 'jquery', which are always loaded.
        'dependencies' => 'jquery.ui.accordion',

        // You need to declare the base path of the file paths in 'scripts' and 'styles'
        'localBasePath' => $extDir,
        // ... and the base from the browser as well. For extensions this is made easy,
        // you can use the 'remoteExtPath' property to declare it relative to where the wiki
        // has $wgExtensionAssetsPath configured:
        'remoteExtPath' => basename( $extDir ),
);


