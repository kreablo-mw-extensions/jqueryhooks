<?php

class JQueryHooks {
	public static function parserFirstCallInit( Parser $parser ) {
		$parser->setHook( 'use_accordion', function ( $input, array $args, Parser $parser, PPFrame $frame ) {

				$parser->getOutput()->addModules( 'ext.JQueryHooks.accordion' );

				return '';
			} );

		return true;
	}
}