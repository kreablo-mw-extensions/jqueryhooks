;(function ($, mw, document) {
	$(document).ready(function() {
		var init = function(c, def, f) {
			$('.' + c).each(function () {
				var $e = $(this);
				var opts = $e.attr('data-options');
				if (typeof(opts) === 'string') {
					opts = JSON.parse(opts);
				}
				if (typeof(opts) !== 'object') {
					opts = def;
				}
				f($e, opts);
			});
		};
		init('akvo-accordion',
			 {
                 collapsible: true,
                 active: false,
                 heightStyle: "content",
             },
			 function ($e, opts) {
				 $e.accordion(opts);
			 });
		init('akvo-slideable', {},
			 function ($e, opts) {
				 $e.on('akvo-toggle-event', function() {
					 $e.slideToggle(opts);
				 });
			 });
		init('akvo-toggle', {},
			 function ($e, opts) {
				 if (typeof(opts.target) !== 'string') {
					 mw.error('.akvo-toggle element must specify a target selector in data-options attribute (e.g., data-options="{&quot;target&quot;: &quot;#id-of-target-element&quot;}")');
					 return;
				 }
				 $e.on('click', function() {
					 $(opts.target).trigger('akvo-toggle-event');
				 });
			 });
	});
})(jQuery, mediaWiki, document);
