<?php
/**
 * Internationalisation file for extension CookieLawInfo
 *
 * @file
 * @ingroup Extensions
 */

$messages = array();

$messages['en'] = array(
    'jqueryhooks' => 'JQuery Hooks',
    'jqueryhooks-desc' => 'Load various jquery widgets for use on wiki pages.',
);

